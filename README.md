# How to install Wordpress with Docker containers in Google Compute Engine

This little guide will help you to install wordpress with mysql in separated docker containers.


**Why this is important?**

With Docker you have the advantage to develop your project in an environment that would be easily deployed in any cloud platform and in any cloud technology such as virtual servers, kubernets or serverless environments.

That means you would be able to move your project across the technology that fits your demand requirements. Initially, the  cheapest option for a low traffic app is Google Compute Engine. 

In the documentation of Compute engine for containers what if found is that you have the option to install a container of a image previously register in Google at the time your create the virtual machine.  However, what I wanted  was to have multiple container in one single virtual machine for that we have to install directly docker and docker-compose in our instance.


Steps:

1. Create a compute engine with linux debian or ubuntu and the option  allow http trafic.
2. Install docker:
	sudo apt-get update
	sudo apt-get install docker-ce docker-ce-cli containerd.io
3. Install docker-compose
	sudo curl -L "https://github.com/docker/compose/releases/download/1.23.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
	sudo chmod +x /usr/local/bin/docker-compose
4. Create a folder where you are going to manage you project. For example: mkdir wordpress
5. Create a file called docker-compose.yml in this place you are going to define how the containers of wordpress and mysql are going to interact.
6. Use the information available in this repository in that file. This file was taken for the docker: https://docs.docker.com/compose/wordpress/
7. run in the console: docker-compose up
8. Go to the IP google has assigned to your virtual machine bat remember use HTTP instead of HTTPS. 
